<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $user = [
            [
                'name'    => 'Nadir Ruayuanda Bakri',
                'address' => 'Perumahan RSCM Blok F no 5',
                'age'     => '21',
            ],
            [
                'name'    => 'Bakri Ruayuanda Nadir',
                'address' => 'Perumahan RSCM Blok F no 6',
                'age'     => '22',
            ],
            [
                'name'    => 'Nadir Bakri',
                'address' => 'Perumahan RSCM Blok F no 7',
                'age'     => '20',
            ],
            
        ];

        return view('welcome', compact('user'));
    }

    public function faculty()
    {
        $faculty = [
            [
                'name'    => 'Fakultas Ilmu Komputer',
                'univ'    => 'Universitas Brawijaya'
            ],
            [
                'name'    => 'Fakultas Teknik',
                'univ'    => 'Universitas Indonesia'
            ],
            [
                'name'    => 'Fakultas Ilmu Budaya',
                'univ'    => 'Universitas Gadjah Mada'
            ],
            
        ];
        return view('faculty', compact('faculty'));
    }

    public function major()
    {
        $major = [
            [
                'name'       => 'Sistem Informasi',
                'faculty'    => 'Fakultas Ilmu Komputer'
            ],
            [
                'name'       => 'Teknik Mesin',
                'faculty'    => 'Fakultas Teknik'
            ],
            [
                'name'       => 'Sastra Arab',
                'faculty'    => 'Fakultas Ilmu Budaya'
            ],
            
        ];
        return view('major', compact('major'));
    }
}
