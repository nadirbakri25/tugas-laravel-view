@extends('layouts.app')

@section('title', '3')

@section('content')
    <h1 class="mt-5">Faculty</h1>

    <table class="table table-dark table-hover mt-5">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">University</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($faculty as $item)
            <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td>{{ $item['name'] }}</td>
                <td>{{ $item['univ'] }}</td>
            </tr>    
            @endforeach
        </tbody>
    </table>
@endsection