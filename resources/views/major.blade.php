@extends('layouts.app')

@section('title', '2')

@section('content')
    <h1 class="mt-5">Major</h1>

    <table class="table table-dark table-hover mt-5">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Faculty</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($major as $item)
            <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td>{{ $item['name'] }}</td>
                <td>{{ $item['faculty'] }}</td>
            </tr>    
            @endforeach
        </tbody>
    </table>
@endsection