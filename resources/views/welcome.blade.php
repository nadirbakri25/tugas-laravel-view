@extends('layouts.app')

@section('title', '1')

@section('content')
    <h1 class="mt-5">User</h1>

    <table class="table table-dark table-hover mt-5">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Address</th>
                <th scope="col">Age</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($user as $item)
            <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td>{{ $item['name'] }}</td>
                <td>{{ $item['address'] }}</td>
                <td>{{ $item['age'] }}</td>
            </tr>    
            @endforeach
        </tbody>
    </table>
@endsection